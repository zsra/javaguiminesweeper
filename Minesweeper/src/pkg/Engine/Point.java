package pkg.Engine;

import java.util.Random;

public class Point {

    public int X;
    public int Y;

    public Point(int x, int y) {

        this.X = x;
        this.Y = y;

    }


    public static Point GetRandomPoint(int x, int y) {

        Random rnd = new Random();
        Point result = new Point(rnd.nextInt(x), rnd.nextInt(y));


        return result;
    }
}
