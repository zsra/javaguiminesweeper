package pkg.Engine;

import pkg.Engine.Point;

public class Engine {

    private static final int DEFAULT_FIELD_SIZE = 10;
    private static final int DEFAULT_MINE_NUMBER = 10;
    private int Size_X;
    private int Size_Y;
    private int MinesNumber;

    /**
     *
     */
    private static int[][] Field;

    public static int[][] GetField() {
        
        return Field;
        
    }
    
    public Engine() {

        this.Size_X = DEFAULT_FIELD_SIZE;
        this.Size_Y = DEFAULT_FIELD_SIZE;
        this.MinesNumber = DEFAULT_MINE_NUMBER;
        Field = new int[this.Size_X][this.Size_Y];

    }

    public Engine(int size_x, int size_y, int mines) {

        this.Size_X = size_x;
        this.Size_Y = size_y;
        this.MinesNumber = mines;
        Field = new int[this.Size_X][this.Size_Y];

    }

    public Engine(int size_x, int size_y) {

        this.Size_X = size_x;
        this.Size_Y = size_y;
        this.MinesNumber = (int)((this.Size_X * this.Size_Y) / 10);
        Field = new int[this.Size_X][this.Size_Y];

    }
    
    

    private void AddMinesToField() {

        for(int mine = 0; mine < this.MinesNumber; mine++) {

            Point p = Point.GetRandomPoint(this.Size_X, this.Size_Y);
            this.Field[p.X][p.Y] = 9;

        }

        System.out.println("=============================================");
        System.out.println("AddMinesToField : DONE");
    }

    private void AddNumbers() {

        int counter = 0;

        for(int x = 0; x < this.Size_X; x++) {

            for(int y = 0; y < this.Size_Y; y++) {

                if(Field[x][y] != 9) {

                    try {

                        if(Field[x-1][y-1] == 9) counter++;


                    } catch(IndexOutOfBoundsException e) {

                    }

                    try {

                        if(Field[x-1][y] == 9) counter++;


                    } catch(IndexOutOfBoundsException e) {

                    }

                    try {

                        if(Field[x-1][y+1] == 9) counter++;


                    } catch(IndexOutOfBoundsException e) {

                    }

                    //-----------------------------------------------------

                    try {

                        if(Field[x][y-1] == 9) counter++;


                    } catch(IndexOutOfBoundsException e) {

                    }

                    try {

                        if(Field[x][y+1] == 9) counter++;


                    } catch(IndexOutOfBoundsException e) {

                    }

                    //------------------------------------------------------

                    try {

                        if(Field[x+1][y-1] == 9) counter++;


                    } catch(IndexOutOfBoundsException e) {

                    }

                    try {

                        if(Field[x+1][y] == 9) counter++;


                    } catch(IndexOutOfBoundsException e) {

                    }

                    try {

                        if(Field[x+1][y+1] == 9) counter++;


                    } catch(IndexOutOfBoundsException e) {

                    }

                    Field[x][y] = counter;
                    counter = 0;

                }

            }

        }

        System.out.println("=============================================");
        System.out.println("AddNumbers : DONE");

    }

    public static void Initialize(int x, int y, int mine) {

                
        Engine _engine = new Engine(x, y, mine);

        _engine.AddMinesToField();
        _engine.AddNumbers();
        //_engine.Print();

        System.out.println("=============================================");
        System.out.println("Initialize  : DONE");

    }
    public static void Initialize(int x, int y) {
        
                
        Engine _engine = new Engine(x, y);

        _engine.AddMinesToField();
        _engine.AddNumbers();
        //_engine.Print();

        System.out.println("=============================================");
        System.out.println("Initialize  : DONE");

    }
    public static void Initialize() {

                
        Engine _engine = new Engine();

        _engine.AddMinesToField();
        _engine.AddNumbers();
        //_engine.Print();

        System.out.println("=============================================");
        System.out.println("Initialize  : DONE");

    }

    private void Print() {

        for(int i = 0; i < this.Size_X; i++) {

            for(int j = 0; j < this.Size_Y; j++) {

                System.out.print(this.Field[i][j]);

            }

            System.out.println();

        }

        System.out.println("=============================================");
        System.out.println("Print  : DONE");

    }
}
