/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import pkg.Engine.Engine;

/**
 *
 * @author hallgato
 */
public class GUI extends JFrame {
    
    private final static int DEFAULT_SIZE_X = 10;
    private final static int DEFAULT_SIZE_Y = 10;
    private final static int DEFAULT_MINE_NUMBER = 10;
    
    private static final long serialVersionUID = 1L;
    private static final JButton[][] Field = 
            new JButton[DEFAULT_SIZE_X][DEFAULT_SIZE_Y];
    private static final JButton Reset = new JButton();
    
    private static Font font = new Font("Courier", Font.BOLD, 16);
    
    public GUI() {
                          
        Init();
        
    }
    
    private void Init() {
    
        Engine.Initialize(DEFAULT_SIZE_X, DEFAULT_SIZE_Y);
        
        JPanel _panel = new JPanel();
        this.add(_panel);
                
        _panel.setLayout(new GridLayout(DEFAULT_SIZE_X + 1, DEFAULT_SIZE_Y));
        _panel.setBackground(Color.WHITE);
        _panel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
        
        
        
        for(int x = 0; x < DEFAULT_SIZE_X; x++) {
        
            for(int y = 0; y < DEFAULT_SIZE_Y; y++) {
            
                final int fx = x;
                final int fy = y;
                
                Field[x][y] = new JButton();
                Field[x][y].setBorder(BorderFactory
                        .createLineBorder(Color.BLACK));
                Field[x][y].setBackground(Color.WHITE);
                Field[x][y].setFont(font);
                _panel.add(Field[x][y]);
                
                Field[x][y].addActionListener((ActionEvent e) -> {
                    Click(fx,fy);
                });
                
                
                Field[x][y].setHorizontalAlignment(JButton.CENTER);
            
            }
            
        }
           
        Reset.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        Reset.setText("Reset");
        
        Reset.addActionListener((ActionEvent e) -> {
            Reset();
        });
        
        _panel.add(Reset);
        
    }
        
    private void Click(int x, int y) {
        
        int[][] values = Engine.GetField();
        if(values[x][y] != 9) {
            
            Field[x][y].setText(Integer.toString(values[x][y]));
            Field[x][y].setBackground(Color.YELLOW);
            
            IsWin(x,y);
                   
        } else {
        
            Field[x][y].setText("X");
            Field[x][y].setForeground(Color.RED);
                        
            Reset(x,y);
                     
        }
    }
       
    private void Reset() {
        
        JOptionPane.showMessageDialog(null,
        "Game over");        
        Init();
        
    }
    
    private void Reset(int x, int y) {
        
        JOptionPane.showMessageDialog(null,
        "Game over");
        
        Field[x][y].setText("");
        Field[x][y].setForeground(Color.BLACK);
        
        Init();
        
    }
    
    private void IsWin(int px, int py) {
        
        int counter = 0;
        
        for(int x = 0; x < DEFAULT_SIZE_X; x++) {
        
            for(int y = 0; y < DEFAULT_SIZE_Y; y++) {
            
                if(Field[x][y].getBackground() == Color.YELLOW) 
                    counter++;
                
            }
            
        }
        
        if(counter == 
                ((DEFAULT_SIZE_X * DEFAULT_SIZE_Y) - DEFAULT_MINE_NUMBER)) {
            
            JOptionPane.showMessageDialog(null,
            "You win!");
            
            counter = 0;
            
            Field[px][py].setText("");            
            Field[px][py].setBackground(Color.WHITE);
            
            Init();
            
        }
        
        counter = 0;           
                
    }
}
